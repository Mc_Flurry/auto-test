
module.exports = {
	content: ["./src/**/*.{html,js}"],
	theme: {
		extend: {
			colors: {
				transparent: 'transparent',
				current: 'currentColor',
				'redrose': '#C13145',
				'lightgrey': '#ECECEC',
			}

		},
		screens: {
			'desktop': { 'min': '1400px' },
			// => @media (max-width: 1535px) { ... }

			'mobile': { 'max': '575px' },
			// => @media (max-width: 639px) { ... }
		}

	},
	plugins: [],
}